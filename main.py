from flask import Flask, request
from flask_restx import Resource, Api


app = Flask(__name__)
api = Api(app)


@api.route('/CrearUsuario')
class CrearUsuario(Resource):
    def post(self):
        return 200,"ok"
        
@api.route('/ValidarUsuario')
class ValidarUsuario(Resource):
    def post(self):
        return 200,"ok"

@api.route('/EditarUsuario')
class EditarUsuario(Resource):
    def put(self):
        return 200,"ok"

@api.route('/AllUsuario')
class AllUsuario(Resource):
    def get(self,todo_id):
        return 200,"ok"

@api.route('/VerUsuario')
class VerUsuario(Resource):
    def get(self):
        return 200,"ok"

@api.route('/EstadoUsuario')
class EstadoUsuario(Resource):
    def get(self,todo_id):
        return 200,"ok"
    def put(self):
        return 200,"ok"

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)